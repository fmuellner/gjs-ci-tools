import {DirWalker} from './dirWalker.js';
import {parseContents, processResults} from './util.js';

import {setConsoleLogDomain} from 'console';
import {exit} from 'system';

setConsoleLogDomain('gjs-check-syntax');

const dirWalker = new DirWalker('.');
dirWalker.addExcludes('.jscheckignore', {optional: true});

const results =
    dirWalker.checkFiles(contents => parseContents(contents));

const status = processResults(results,
    'The following files contain invalid syntax',
    r => `${r.file}: ${r.error.message}`);
exit(status);
