// This regular expression is borrowed from Vite for detecting modules
// https://github.com/vitejs/vite/blob/40d503cce08324b6bd6341b45ad8a73876f6f4a0/packages/vite/src/node/optimizer/scan.ts
const importsRE =
    /(?<!\/\/.*)(?<=^|;|\*\/)\s*import(?!\s+type)(?:[\w*{}\n\r\t, ]+from\s*)?\s*("[^"]+"|'[^']+')\s*(?=$|;|\/\/|\/\*)/m;

const exportsRE = /export[^A-z0-9.]/m;

import GLib from 'gi://GLib';
import Gio from 'gi://Gio';
import {programInvocationName} from 'system';

export function parseContents(contents) {
    const parseOptions = {};
    if (importsRE.test(contents) || exportsRE.test(contents))
        parseOptions.target = 'module';
    return Reflect.parse(contents, parseOptions);
}

/**
 * @param {CheckResult[]} results - list of results
 * @param {string} title - heading for output list
 * @param {(CheckResult) => string} errorPrinter - function to transform result
 * @returns {number} - status code
 */
export function processResults(results, title, errorPrinter) {
    writeJunitReport(results);

    const failures = results.filter(r => r.error);

    if (failures.length === 0)
        return 0;

    printerr(`${title}:`);
    failures.map(r => `  ${errorPrinter(r)}`).forEach(str => printerr(str));
    return 1;
}

function writeJunitReport(results) {
    const suiteName =
        GLib.path_get_basename(programInvocationName).replace('.js', '');
    const nTests = results.length;
    const nFailures = results.filter(r => r.error).length;

    function toTestcase(r) {
        const caseAttrs = `name="${r.file}" classname="${suiteName}"`;

        if (!r.error)
            return `<testcase ${caseAttrs}/>`;

        const failure = GLib.markup_escape_text(r.error.message, -1);
        return `<testcase ${caseAttrs}><failure message="${failure}"/></testcase>`;
    }

    const output = `<testsuites>
<testsuite name="${suiteName}" tests="${nTests}" failures="${nFailures}">
${results.map(toTestcase).join('\n')}
</testsuite>
</testsuites>
`;

    const file = Gio.File.new_for_commandline_arg(`${suiteName}.junit.xml`);
    file.replace_contents(output, null, false, Gio.FileCreateFlags.NONE, null);
}

/**
 * @param {object} node - a node in the AST
 * @param {Function} func - function to call on each node
 */
export function walkAst(node, func) {
    func(node);
    nodesToWalk(node).forEach(n => {
        if (checkNode(n, node))
            walkAst(n, func);
    });
}

const warnedNodes = new Set();
function checkNode(node, parent) {
    if (node)
        return true;

    const {type} = parent;
    if (!warnedNodes.has(type))
        console.warn(`Node "${type}" returned invalid child node "${node}"`);
    warnedNodes.add(type);
    return false;
}

function nodesToWalk(node) {
    switch (node.type) {
    case 'ArrayPattern':
    case 'BreakStatement':
    case 'CallSiteObject':  // i.e. strings passed to template
    case 'ContinueStatement':
    case 'DebuggerStatement':
    case 'EmptyStatement':
    case 'Identifier':
    case 'Literal':
    case 'MetaProperty':  // i.e. new.target
    case 'Super':
    case 'ThisExpression':
    case 'ImportSpecifier':
    case 'ImportNamespaceSpecifier':
    case 'CallImport':
        return [];
    case 'ArrowFunctionExpression':
    case 'FunctionDeclaration':
    case 'FunctionExpression':
        return [...node.defaults, node.body].filter(n => !!n);
    case 'AssignmentExpression':
    case 'BinaryExpression':
    case 'ComprehensionBlock':
    case 'LogicalExpression':
        return [node.left, node.right];
    case 'ArrayExpression':
    case 'TemplateLiteral':
        return node.elements.filter(n => !!n);
    case 'BlockStatement':
    case 'Program':
        return node.body;
    case 'StaticClassBlock':
        return [node.body];
    case 'ClassField':
        return [node.name, node.init].filter(n => !!n);
    case 'CallExpression':
    case 'NewExpression':
    case 'OptionalCallExpression':
    case 'TaggedTemplate':
        return [node.callee, ...node.arguments];
    case 'CatchClause':
        return [node.body, node.guard].filter(n => !!n);
    case 'ClassExpression':
    case 'ClassStatement':
        return [...node.body, node.superClass].filter(n => !!n);
    case 'ClassMethod':
        return [node.name, node.body];
    case 'ComprehensionExpression':
    case 'GeneratorExpression':
        return [node.body, ...node.blocks, node.filter].filter(n => !!n);
    case 'ComprehensionIf':
        return [node.test];
    case 'ComputedName':
        return [node.name];
    case 'ConditionalExpression':
    case 'IfStatement':
        return [node.test, node.consequent, node.alternate].filter(n => !!n);
    case 'DoWhileStatement':
    case 'WhileStatement':
        return [node.body, node.test];
    case 'ExportDeclaration':
        return [node.declaration, node.source].filter(n => !!n);
    case 'ImportDeclaration':
        return [...node.specifiers, node.moduleRequest];
    case 'LetStatement':
        return [...node.head, node.body];
    case 'ExpressionStatement':
        return [node.expression];
    case 'ForInStatement':
    case 'ForOfStatement':
        return [node.body, node.left, node.right];
    case 'ForStatement':
        return [node.init, node.test, node.update, node.body].filter(n => !!n);
    case 'LabeledStatement':
        return [node.body];
    case 'MemberExpression':
        return [node.object, node.property];
    case 'ModuleRequest':
        return [node.source];
    case 'ObjectExpression':
    case 'ObjectPattern':
        return node.properties;
    case 'OptionalExpression':
        return [node.expression];
    case 'OptionalMemberExpression':
        return [node.object, node.property];
    case 'Property':
    case 'PrototypeMutation':
        return [node.value];
    case 'ReturnStatement':
    case 'ThrowStatement':
    case 'UnaryExpression':
    case 'UpdateExpression':
    case 'YieldExpression':
        return node.argument ? [node.argument] : [];
    case 'SequenceExpression':
        return node.expressions;
    case 'SpreadExpression':
        return [node.expression];
    case 'SwitchCase':
        return [node.test, ...node.consequent].filter(n => !!n);
    case 'SwitchStatement':
        return [node.discriminant, ...node.cases];
    case 'TryStatement':
        return [node.block, node.handler, node.finalizer].filter(n => !!n);
    case 'VariableDeclaration':
        return node.declarations;
    case 'VariableDeclarator':
        return node.init ? [node.init] : [];
    case 'WithStatement':
        return [node.object, node.body];
    default:
        print(`Ignoring ${node.type}, you should probably fix this in the script`);
        console.debug(node);
        return [];
    }
}
