# gjs-ci-tools

A collection of gjs script for use in continuous integration.

- ### gjs-check-syntax

    Try to load and parse all .js scripts or modules under the current
    directory, and report any failures.

    It is meant as a replacement for `js91 --compileonly`.

- ### gjs-check-potfiles

    Find any .js scripts or modules under the current directory, and
    report all that aren't listed in either `po/POTFILES.in` or
    `po/POTFILES.skip` but call any of the gettext functions.
