import {DirWalker} from './dirWalker.js';
import {parseContents, processResults, walkAst} from './util.js';

import {setConsoleLogDomain} from 'console';
import {exit} from 'system';

const gettextFuncs = new Set([
    '_',
    'N_',
    'C_',
    'NC_',
    'dcgettext',
    'dgettext',
    'dngettext',
    'dpgettext',
    'gettext',
    'ngettext',
    'pgettext',
]);

class GettextError extends Error {
    name = 'GettextError';
    message = 'File is missing from po/POTFILES.in';
}

function findGettextCalls(node) {
    switch (node.type) {
    case 'CallExpression':
        if (node.callee.type === 'Identifier' &&
            gettextFuncs.has(node.callee.name))
            throw new GettextError();
        if (node.callee.type === 'MemberExpression' &&
            node.callee.object.type === 'Identifier' &&
            node.callee.object.name === 'Gettext' &&
            node.callee.property.type === 'Identifier' &&
            gettextFuncs.has(node.callee.property.name))
            throw new GettextError();
        break;
    }
}

setConsoleLogDomain('gjs-check-potfiles');

const dirWalker = new DirWalker('.');
dirWalker.addExcludes('po/POTFILES.in');
dirWalker.addExcludes('po/POTFILES.skip', {optional: true});

const results =
    dirWalker.checkFiles(contents => {
        try {
            const ast = parseContents(contents);
            walkAst(ast, findGettextCalls);
        } catch (e) {
            if (e instanceof GettextError)
                throw e;
            console.warn(`Error while parsing file: ${e.message}`);
        }
    });

const status = processResults(results,
    'The following files are missing from po/POTFILES.in',
    r => r.file);
exit(status);
