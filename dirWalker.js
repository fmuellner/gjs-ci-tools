import Gio from 'gi://Gio';
import GLib from 'gi://GLib';

export class DirWalker {
    #root;
    #excludedFiles = new Set();
    #results = new Map();
    #decoder = new TextDecoder();

    constructor(root) {
        this.#root = Gio.File.new_for_commandline_arg(root);
    }

    /**
     * @param {string} filename — file to read excluded files from
     * @param {object} options — options
     * @param {boolean} options.optional — whether the file is optional
     */
    addExcludes(filename, options = {}) {
        const {optional = false} = options;

        try {
            const [, contents] = GLib.file_get_contents(filename);
            this.#decoder.decode(contents).split('\n')
                .filter(line => line && !line.startsWith('#'))
                .forEach(line => this.#excludedFiles.add(line));
        } catch (e) {
            if (optional && e.matches(GLib.FileError, GLib.FileError.NOENT))
                return;
            console.warn(`Failed to read ${filename}: ${e.message}`);
        }
    }

    #walkDir(dir, checkFunc) {
        const dirEnum = dir.enumerate_children(
            'standard::name,standard::type', 0, null);
        while (true) {
            const info = dirEnum.next_file(null);
            if (!info)
                break;

            const name = info.get_name();
            if (name.startsWith('.'))
                continue;

            const child = dirEnum.get_child(info);
            const relativePath = this.#root.get_relative_path(child);
            if (this.#excludedFiles.has(relativePath))
                continue;

            if (info.get_file_type() === Gio.FileType.DIRECTORY) {
                this.#walkDir(child, checkFunc);
                continue;
            }

            if (!name.endsWith('.js'))
                continue;

            try {
                const [, contents] = GLib.file_get_contents(relativePath);
                const script = this.#decoder.decode(contents);
                checkFunc(script);
                this.#results.set(relativePath, null);
            } catch (e) {
                this.#results.set(relativePath, e);
            }
        }
    }

    /**
     * @typedef {object} CheckResult
     * @property {string} file — file that was checked
     * @property {?Error} error — error that occurred (if any)
     */

    /**
     * @param {(contents: string) => void} checkFunc — check to perform
     *   on each file
     * @returns [CheckResult] — list of results
     */
    checkFiles(checkFunc) {
        this.#results.clear();
        this.#walkDir(this.#root, checkFunc);
        return [...this.#results].map(([file, error]) => ({file, error}));
    }
}
